'use strict';

const MongoClient = require('mongodb').MongoClient;

const { loadEnv } = require('./environment');
loadEnv();

let connection;

async function connect() {
  if (!connection) {
    connection = await MongoClient.connect(process.env.MONGODB_URI, { useUnifiedTopology: true });
  } else {
    return connection;
  }
}

async function close() {
  connection && connection.close();
}

const getConnection = () => connection;
const getDatabase = () => connection.db();

module.exports = { connect, close, getConnection, getDatabase };
