'use strict';

const { getDatabase } = require('../configs/database');

exports.getRecordsByFilter = async (params) => {
  return new Promise((resolve, reject) => {
    const { startDate, endDate, minCount, maxCount } = params;
    const db = getDatabase();
    const collection = db.collection('records');

    collection.aggregate([
      {
        $project: {
          _id: 0,
          key: "$key",
          createdAt: "$createdAt",
          totalCount: { $sum: "$counts" }
        },
      },
      {
        $match: {
          totalCount: {
            $gte: minCount,
            $lte: maxCount
          },
          createdAt: {
            $gte: new Date(startDate),
            $lte: new Date(endDate)
          }
        },
      }
    ]).toArray(function (err, docs) {
      if (err) {
        return reject(err);
      }
      resolve(docs);
    });
  });
};
