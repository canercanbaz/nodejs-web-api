'use strict';

const express = require('express');

const database = require('./configs/database');
const { loadEnv } = require('./configs/environment');
loadEnv();

const app = express();

async function main() {
  try {
    if (process.env.NODE_ENV !== 'test') {
      await database.connect();
    }
  } catch (error) {
    console.error(`Could not connect to the database: ${error}`);
    process.exit();
  };

  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  // Load routes
  require('./routes')(app);
}

main();

module.exports = app;
