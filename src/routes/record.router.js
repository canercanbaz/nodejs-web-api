'use strict';

const express = require('express');
const router = express.Router();
const recordController = require('../controllers/record.controller');

router.post('/', recordController.getRecords);

module.exports = router;
