'use strict';

const response = require('../utils/response');

module.exports = (app) => {
  app.use('/api/records', require('./record.router'));

  // 404 not found route - Keep this as the last route!
  app.get('*', (_, res) => {
    response.notFoundError(res, { msg: 'Not found' });
  });
};
