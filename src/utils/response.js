'use strict';

//Returns 200 Success
exports.success = (res, data) => {
  return res.status(200).json(data);
};

//Returns 500 Internal Server Error
exports.serverError = (res, error) => {
  return res.status(500).json(error);
};

//Returns 400 Validation Error
exports.validationError = (res, error) => {
  return res.status(400).json(error);
};

//Returns 404 Not Found Error
exports.notFoundError = (res, error) => {
  return res.status(404).json(error);
};
