'use strict';

const { getRecordsByFilter } = require('../services/record.service');
const response = require('../utils/response');
const { isEmpty } = require('../utils/validation');

exports.getRecords = async (req, res) => {
  try {
    const { startDate, endDate, minCount, maxCount } = req.body;
    let error = {
      code: 1,
      msg: '',
    };

    if (
      isEmpty(startDate) ||
      isEmpty(endDate) ||
      isEmpty(minCount) ||
      isEmpty(maxCount)
    ) {
      error.msg = 'Validation Error: startDate, endDate, minCount, maxCount parameters need to be provided';
      return response.validationError(res, error);
    }

    if (typeof minCount !== 'number' || typeof maxCount !== 'number') {
      error.msg = 'Validation Error: minCount and maxCount have to be an integer.';
      return response.validationError(res, error);
    }

    if (minCount < 0) {
      error.msg = 'Validation Error: minCount cannot have a negative value';
      return response.validationError(res, error);
    }

    if (minCount > maxCount) {
      error.msg = 'Validation Error: minCount cannot be greater than maxCount';
      return response.validationError(res, error);
    }

    if (new Date(startDate).getTime() > new Date(endDate).getTime()) {
      error.msg = 'Validation Error: startDate cannot be greater than endDate';
      return response.validationError(res, error);
    }

    const records = await getRecordsByFilter({ startDate, endDate, minCount, maxCount });
    const data = {
      code: 0,
      msg: 'Success',
      records: records
    };
    return response.success(res, data);
  } catch (error) {
    return response.serverError(res, 'Could not get records');
  }
};
