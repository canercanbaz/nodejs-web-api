# Node.js Web API

### Environment Variables

You need to create a .env file for the environment variables for the api to work correctly.  
For the tests, .env.test file need to be created.  
Sample environment file is below:

```bash
PORT=3000
MONGODB_URI=mongodb_uri
```

### How to start?

1. Install npm modules by runnning: `npm install` in the root folder.
1. Run the server according to the environment
* Production: `npm run start`
* Development: `npm run dev`

### How to run tests?

You can run `npm test` command to start testing.

### Sample Request/Response Body

```json
{
    "startDate": "2016-01-01",
    "endDate": "2018-12-31",
    "minCount": 0,
    "maxCount": 100
}
```

The result will look like following:

```json
{
    "code": 0,
    "msg": "Success",
    "records": [
        {
            "key": "TAKwGc6Jr4i8Z487",
            "totalCount": 56,
            "createdAt": "2017-01-28T01:22:14.398Z"
        }
    ]
}
```

### Notes

Node v12.20.1 and npm v6.14.10 were used for development.