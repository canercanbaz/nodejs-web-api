const { getRecordsByFilter } = require('../../src/services/record.service');

describe('Record Service', function () {
  describe('getRecordsByFilter', function () {
    it('returns records as an array', async () => {
      const params = {
        startDate: "2016-01-01",
        endDate: "2018-12-31",
        minCount: 0,
        maxCount: 1000
      };
      const records = await getRecordsByFilter(params);
      expect(records.length).toBeGreaterThan(0);
    });

    it('throws error when parameters are not provided', async (done) => {
      try {
        await getRecordsByFilter();
        done.fail(new Error('It should throw an error when parameters are not provided'));
      } catch (error) {
        expect(error).toBeDefined();
        done();
      }
    });
  });
});
