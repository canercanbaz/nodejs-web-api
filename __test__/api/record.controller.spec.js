const app = require('../../src/app');
const { post } = require('../helpers/requestHelpers');

describe('Record Controller', function () {
  describe('POST /api/records', function () {
    it('returns records successfully', async (done) => {
      const requestBody = {
        startDate: "2016-01-01",
        endDate: "2016-02-01",
        minCount: 0,
        maxCount: 1000
      };
      post(app, `/api/records`, requestBody, response => {
        expect(response.body.code).toBe(0); // Success
        expect(response.body.msg).toBe('Success');
        expect(response.statusCode).toBe(200);
        done();
      });
    });

    it('throws error when one of the parameters is missing', async (done) => {
      // maxCount is missing
      const requestBody = {
        startDate: "2016-01-01",
        endDate: "2016-02-01",
        minCount: 0
      };
      post(app, `/api/records`, requestBody, response => {
        expect(response.body.code).toBe(1); // Error
        expect(response.body.msg).toMatch(/Validation/);
        expect(response.statusCode).toBe(400);
        done();
      });
    });

    it('throws error when minCount or maxCount is not an integer', async (done) => {
      const requestBody = {
        startDate: "2016-01-01",
        endDate: "2016-02-01",
        minCount: true,
        maxCount: 10
      };
      post(app, `/api/records`, requestBody, response => {
        expect(response.body.code).toBe(1); // Error
        expect(response.body.msg).toMatch(/Validation/);
        expect(response.statusCode).toBe(400);
        done();
      });
    });

    it('throws error when minCount is greater than the maxCount', async (done) => {
      const requestBody = {
        startDate: "2016-01-01",
        endDate: "2016-02-01",
        minCount: 100,
        maxCount: 10
      };
      post(app, `/api/records`, requestBody, response => {
        expect(response.body.code).toBe(1); // Error
        expect(response.body.msg).toMatch(/Validation/);
        expect(response.statusCode).toBe(400);
        done();
      });
    });

    it('throws error when minCount has a negative value', async (done) => {
      const requestBody = {
        startDate: "2016-01-01",
        endDate: "2016-02-01",
        minCount: -20,
        maxCount: 10
      };
      post(app, `/api/records`, requestBody, response => {
        expect(response.body.code).toBe(1); // Error
        expect(response.body.msg).toMatch(/Validation/);
        expect(response.statusCode).toBe(400);
        done();
      });
    });

    it('throws error when startDate is greater than the endDate', async (done) => {
      const requestBody = {
        startDate: "2016-01-01",
        endDate: "2016-02-01",
        minCount: -20,
        maxCount: 10
      };
      post(app, `/api/records`, requestBody, response => {
        expect(response.body.code).toBe(1); // Error
        expect(response.body.msg).toMatch(/Validation/);
        expect(response.statusCode).toBe(400);
        done();
      });
    });
  });
});
