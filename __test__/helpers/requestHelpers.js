const request = require('supertest');

exports.post = async function (app, path, data, then) {
  request(app)
    .post(path)
    .send(data)
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .then(async (response) => {
      then(response);
    });
};
