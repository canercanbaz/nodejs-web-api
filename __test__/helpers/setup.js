const database = require('../../src/configs/database');

beforeAll(async done => {
  await database.connect();
  done();
});

afterAll(async done => {
  await database.close();
  done();
});